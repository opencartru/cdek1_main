﻿Если у вас установлена версия модуля ниже 1.2 то для сохранения всех  в системе отгрузок нужно запустить обновить базу данных.

1) Запустить phpMyAdmin для редактирования базы данных.
2) Скопировать в поле запроса следующий код:

ALTER TABLE  `oc_cdek_order` ADD  `cod` FLOAT(8, 4) NOT NULL DEFAULT '0.0000' AFTER `delivery_recipient_cost`;
ALTER TABLE  `oc_cdek_order` ADD  `cod_fact` FLOAT(8, 4) NOT NULL DEFAULT '0.0000' AFTER `cod`;
ALTER TABLE  `oc_cdek_order` ADD  `currency` VARCHAR(3) NOT NULL DEFAULT 'RUB' AFTER `delivery_recipient_name`;
ALTER TABLE  `oc_cdek_order` ADD  `currency_cod` VARCHAR(3) NOT NULL DEFAULT 'RUB' AFTER `currency`;
CREATE TABLE IF NOT EXISTS `oc_order_to_sdek` (
  `order_to_sdek_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `pvz_code` varchar(255) NOT NULL,
  PRIMARY KEY (`order_to_sdek_id`),
  UNIQUE KEY `order_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

3) Нажмите кнопку "ОК".

ВНИМАНИЕ! Если вы используете префикс таблиц в базе данных (файл config.php в корне сайта ключ DB_PREFIX) отличный от 'oc_', то в указанных запросах заменить oc_ на ваш префикс.